//
//  UserListCell.swift
//  UserList
//
//  Created by Amol Tamboli on 06/10/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class UserListCell: UITableViewCell {

    @IBOutlet weak var lblfirstletter: UILabel!
    @IBOutlet weak var lblemail: UILabel!
    @IBOutlet weak var lblName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    func setData(cUserListData: UserListData)
    {
        self.lblName.text = cUserListData.name
        self.lblemail.text = cUserListData.email
        self.lblfirstletter.text = String((cUserListData.name?.first)!)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
