//
//  UserListVC.swift
//  UserList
//
//  Created by Amol Tamboli on 06/10/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class UserListVC: UIViewController,UISearchBarDelegate
{
    @IBOutlet weak var m_cSearchBar       : UISearchBar!
    @IBOutlet weak var m_cUserListTblView : UITableView!
    
    var userDetail = [UserListData]()
    var userList = [UserListData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         
        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.isTranslucent = false

        self.m_cUserListTblView.register(UINib(nibName: "UserListCell", bundle: nil), forCellReuseIdentifier: "UserListCell")
        
        self.m_cUserListTblView.dataSource = self
        self.m_cUserListTblView.delegate   = self
        self.m_cSearchBar.delegate         = self
        
        self.m_cUserListTblView.tableFooterView = UIView()
        self.view.makeToastActivity(.center)
        self.getUserlist()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        #if DEV
        self.title = "UserInfo"
        
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.86, green: 0.24, blue: 0.33, alpha: 1.00)
        
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        
        #else
        self.title = "UserList"
        navigationController?.navigationBar.barTintColor = UIColor(red: 0.14, green: 0.40, blue: 0.87, alpha: 1.00)
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
        #endif
    }

 //MARK: - FUNCTIONS

 
    func getUserlist() -> Void{
        
        
        HttpClientApi.instance().makeAPICall(strAPI: Constant.Users, params: nil, method: .GET, success: { (Data) in
            OperationQueue.main.addOperation {
                do
                {
                    let Usermodel = try JSONDecoder().decode([UserListData].self, from: Data)
                    print(Usermodel)
                    self.userDetail = Usermodel
                    self.userList = Usermodel
                    self.view.hideToastActivity()
                    self.m_cUserListTblView.reloadData()
                    
                }catch
                {
                    print(error)
                }
            }
        }) { (reasponse) in
            
        }
    }
    
    //MARK : - SearchBar Delegate Method
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        
        guard !searchText.isEmpty else {
            self.userDetail = self.userList
            self.m_cUserListTblView.reloadData()
            return
        }
        
        self.userDetail = self.userList.filter ({ cUserDetail -> Bool in
            (cUserDetail.name?.lowercased().contains(searchText.lowercased()))!
        })
        
        self.m_cUserListTblView.reloadData()
    }
}

// MARK : - extension

extension UserListVC: TableViewDataSourceDelegate
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let lcUserListcell = m_cUserListTblView.dequeueReusableCell(withIdentifier: "UserListCell", for: indexPath) as! UserListCell
        
        lcUserListcell.setData(cUserListData: self.userDetail[indexPath.row])
        
        return lcUserListcell
            
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let lcDetailVc = storyboard?.instantiateViewController(withIdentifier: "UserListDetailsVC") as! UserListDetailsVC
        lcDetailVc.m_cUserDetails = self.userDetail[indexPath.row]
        self.navigationController?.pushViewController(lcDetailVc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
}
