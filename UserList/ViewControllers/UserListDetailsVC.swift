//
//  UserListDetailsVC.swift
//  UserList
//
//  Created by Amol Tamboli on 06/10/20.
//  Copyright © 2020 Amol Tamboli. All rights reserved.
//

import UIKit

class UserListDetailsVC: UIViewController {

    
    @IBOutlet weak var m_cName    : UILabel!
    @IBOutlet weak var m_cEmail   : UILabel!
    @IBOutlet weak var m_cAddress : UILabel!
    @IBOutlet weak var m_cPhone   : UILabel!
    @IBOutlet weak var m_cCompany : UILabel!
    
    var m_cUserDetails: UserListData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "UserDetails"
        // Do any additional setup after loading the view.
        self.m_cName.text = self.m_cUserDetails.name
        self.m_cEmail.text = self.m_cUserDetails.email
        self.m_cAddress.text = "\(self.m_cUserDetails!.address!.street!), \(self.m_cUserDetails!.address!.suite) \(self.m_cUserDetails!.address!.city!) \(self.m_cUserDetails!.address!.zipcode!)"
        self.m_cPhone.text = self.m_cUserDetails.phone
        self.m_cCompany.text = self.m_cUserDetails.company?.name
    }
    
}
