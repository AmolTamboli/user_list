//
//  ApiServices.swift
//  EquinoxApp
//
//  Created by Amol Tamboli on 7/10/19.
//  Copyright © 2019 Amol Tamboli. All rights reserved.
//


import UIKit
typealias TableViewDataSourceDelegate = UITableViewDataSource  & UITableViewDelegate

enum HttpMethode:String {
    case POST
    case GET
    case DELETE
    case PUT
}

struct Constant {
    // Production URL
    static let baseURL = "https://jsonplaceholder.typicode.com/"
    static let Users = "users"
  
}


class HttpClientApi: NSObject {
    
    var request : URLRequest?
    var session :URLSession?
    var strContentType : String = ""
    
    static func instance()-> HttpClientApi{
        return HttpClientApi()
    }
    
    func makeAPICall(strAPI: String,params: Dictionary<String, Any>!, method: HttpMethode, success:@escaping (Data ) -> Void, failure: @escaping ( Any )-> Void) {
        
        
        let fileUrl = NSURL(string:Constant.baseURL + strAPI)! as URL
        let request = NSMutableURLRequest(url:fileUrl as URL);
        let strParameters = NSMutableString()
        if let params = params{
            for (keys, obj) in params{
                strParameters.append("&\(keys)=\(obj)")
            }
        }
        let data = strParameters.data(using: String.Encoding.utf8.rawValue)
        request.httpBody = data
        request.httpMethod = HttpMethode.GET.rawValue
//        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) {
            data, response, error in
            guard let data = data, error == nil else {
                print(error ?? "Unknown error")
//                if withHUD {
//                    OperationQueue.main.addOperation {
//                        SVProgressHUD.dismiss()
//                    }
//                }
                return
            }
            success(data)
//            if withHUD {
//                OperationQueue.main.addOperation {
//                    SVProgressHUD.dismiss()
//                }
//            }
        }
        task.resume()
    }
    
}


  
